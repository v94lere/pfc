# Public Practical Works Repository

This repository contains a collection of practical works carried out publicly. Each practical work is organized in a separate directory and includes the necessary files and instructions to complete the respective task.

## List of Practical Works

1. [pomodoro-timer_python](./pomodoro-timer_python/)
   - Description: Introduction to basic Python concepts.
   - Files: `timer.py`, `README.md`.

2. [calculator_python](./calculator_python/)
   - Description: Manipulating lists, tuples, and dictionaries in Python.
   - Files: `main.py`, `README.md`.

3. [dice-simulator_python](./dice-simulator_python/)
   - Description: Introduction to object-oriented programming (OOP) in Python.
   - Files: `dice_simulator.py`, `README.md`.

4. [rock-paper-scissors](./rock-paper-scissors/)
   - Description: Introduction to object-oriented programming (OOP) in Python.
   - Files: `rockpaperscissors.py`, `test_rockpaperscissors.py`, `README.md`.

5. [pizza_python](./pizza_python/)
   - Description: Introduction to object-oriented programming (OOP) in Python.
   - Files: `pizza.py`, `test_pizza.py`, `README.md`.

---

© 2024 v94lere